#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include "list.h"
#include <cstdlib>
#include <ctime>

#include <cstdio>
void* operator new(size_t sz) {return malloc(sz);}
void operator delete(void* ptr) noexcept {free(ptr);}

typedef unsigned char u8;
typedef unsigned short rpx;
typedef signed long pos;

namespace font {
	typedef u8 char_t[5*5];
	char_t amount = {
		0,1,1,1,0,
		0,0,1,0,0,
		0,0,1,0,0,
		0,0,1,0,0,
		1,1,1,1,1
	};
	char_t water = {
		1,0,1,1,1,
		1,0,1,0,1,
		1,0,1,0,1,
		1,0,0,0,1,
		1,1,1,1,1
	};
	char_t dirt = {
		1,1,0,1,1,
		0,1,1,1,0,
		0,1,0,1,0,
		0,1,0,1,0,
		1,1,0,1,1,
	};
	char_t tree = {
		1,0,0,0,1,
		1,1,1,1,1,
		0,0,1,0,0,
		0,1,1,1,0,
		0,0,1,0,0
	};
	char_t good = {
		1,1,1,1,1,
		1,0,0,0,1,
		1,0,0,1,1,
		1,0,0,0,0,
		1,1,1,1,1
	};
	char_t make = {
		1,1,1,1,1,
		0,0,1,0,0,
		0,1,1,1,0,
		0,0,1,0,0,
		1,1,1,1,1
	};
	char_t food = {
		1,1,1,1,1,
		1,0,0,0,1,
		1,1,1,1,1,
		1,0,0,0,1,
		1,1,1,1,1
	};
	char_t field = {
		0,1,1,1,0,
		0,0,0,0,0,
		1,1,1,1,1,
		0,0,0,0,0,
		0,1,1,1,0
	};
	char_t fire = {
		0,0,1,0,0,
		0,0,1,0,0,
		1,1,1,1,1,
		0,1,0,1,0,
		0,1,0,1,0,
	};
	char_t caus = {
		0,0,1,0,0,
		1,1,1,1,1,
		0,0,0,0,0,
		1,1,1,1,1,
		1,0,1,0,1,
	};
	char_t have = {
		0,0,1,0,0,
		1,1,1,1,1,
		0,0,0,0,0,
		1,1,1,1,1,
		1,0,1,0,1,
	};
	char_t agent = {
		0,0,1,0,0,
		1,1,1,1,1,
		1,0,0,0,1,
		1,1,1,1,1,
		0,0,1,0,0,
	};
	char_t instr = {
		0,0,1,0,0,
		1,0,1,0,1,
		1,0,1,0,1,
		1,1,1,1,1,
		0,0,1,0,0,
	};
	char_t p1sg = {
		1,0,1,0,1,
		1,0,0,0,1,
		1,1,1,1,1,
		0,0,1,0,0,
		0,1,1,1,0,
	};
	char_t p2sg = {
		1,1,1,1,1,
		0,0,1,0,0,
		1,1,1,1,1,
		1,0,1,0,1,
		0,1,1,1,0,
	};
	char_t cop = {
		1,1,1,1,1,
		0,1,0,1,0,
		0,0,0,0,0,
		0,1,0,1,0,
		1,1,1,1,1,
	};	
	char_t adj = {
		0,1,0,0,0,
		0,1,1,1,0,
		0,0,0,1,0,
		0,1,1,1,0,
		0,1,0,1,0,
	};
	char_t prod = {
		0,0,0,1,0,
		0,1,1,1,1,
		0,1,0,1,0,
		1,1,1,1,0,
		0,1,0,0,0,
	};
	char_t bush = {
		0,1,1,1,0,
		0,1,0,1,0,
		1,1,1,1,1,
		0,1,0,1,0,
		0,1,0,1,0,
	};
	// food-water = fish
	// dirt-water = sand
	// amount-good = hp
	// amount-food = hunger
	// food-field = wheat
	// food-field-fire = bread
	// fire-caus = start a fire
	// have-caus = get
	// fire-caus-agent = firestarter, arsonist (person)
	// fire-caus-instr = firestarter (tool)
	// dirt-adj-good = grassland
	// prod-tree = wood
	char_t* chars[] = {
		&amount, // 3
		&water,  // 4
		&tree,   // 5
		&good,   // 6
		&make,   // 7
		&food,   // 8
		&field,  // 9
		&fire,   // 10
		&caus,   // 11
		&have,   // 12
		&agent,  // 13
		&instr,  // 14
		&p1sg,   // 15
		&p2sg,   // 16
		&cop,    // 17
		&adj,    // 18
		&prod,   // 17
		&bush,   // 18
	}; 
	void drawchar(rpx px, rpx py, u8 size, size_t i) {
		char_t& c = *chars[i];
		for (u8 x = 0; x < 5; x++) {
			for (u8 y = 0; y < 5; y++) {
				if (c[x + y*5]) {
					glBegin(GL_QUADS);
					glVertex2f(px+x*size, py-y*size);
					glVertex2f(px+x*size, (py-y*size)-size);
					glVertex2f(px+x*size+size, (py-y*size)-size);
					glVertex2f(px+x*size+size, py-y*size);
					glEnd();
				}
			}
		}
	}
	bool drawstring(rpx x, rpx y, rpx w, rpx h, u8 size, const char* str) {
		rpx xp=0, yp=0;
		while (*str!=0) {
			if (*str == 1) // space
				xp+=size*2;
			else if (*str == 2) { // newline
				yp+=size*8;
				goto newline;
			} else {
				drawchar(x+xp,y-yp,size,*str-3);
				xp+=size*6;
			}
			if (xp + size > w) {
				yp+=size*6;
			newline:
				xp=0;
				if (yp > h) return false;
			}
			++str;
		}
		return true;
	}
	struct bounds_t {rpx x, y;};
	bounds_t bounds(u8 size, const char* str) {
		rpx xp=0, yp=0;
		while (*str!=0) {
			if (*str == 1) // space
				xp+=size*2;
			else if (*str == 2) { // newline
				yp+=size*8;
				xp=0;
			} else {
				xp+=size*6;
			}
			++str;
		}
		bounds_t b = {xp,(rpx)(yp+size*5)};
		return b;
	}
}
namespace game { void draw(), update(); }
namespace runtime {
	rpx width;
	rpx height;
	double lastframe;
	GLFWwindow* window;
	void resize(GLFWwindow*, int w, int h) {
		width = w;
		height = h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,width,height);
		glOrtho(0,width,0,height,-1,1);
		glMatrixMode(GL_MODELVIEW);
		glfwSwapInterval(1);
		glLoadIdentity();
	}
	void init(rpx w, rpx h) {
		glfwInit();
		glfwWindowHint(GLFW_SAMPLES, 8);
		window = glfwCreateWindow(w,h,"acquisition",NULL,NULL);
		glfwSetWindowSizeCallback(window,resize);
		glfwMakeContextCurrent(window);
		resize(nullptr,w,h);
		lastframe = 0;
	}

	void loop() {
		while(!glfwWindowShouldClose(window)) {
			double ticks = glfwGetTime();
			if (ticks-lastframe > .05) {
				game::update();
				glClear(GL_COLOR_BUFFER_BIT);
				glClearColor(.2,.2,.2,1);
				game::draw();
				glfwSwapBuffers(window);
				lastframe = glfwGetTime();
			}
			glfwPollEvents();
		}
	}
}
namespace game {
	typedef unsigned short hp_t;
	struct entity {
		pos x, y;
		enum class kind {person, bigtree, tree, rock, shrub} kind;
		const static char* names[];
		hp_t hp;
		union {
			struct {
				hp_t hydration;
				hp_t food;
				hp_t energy;
			};
		};
	};
	const char* entity::names[] = {"\xe","\x5\x12\x6","\5","","\x14"};
	enum class tile {
		// impassable
			water,
		// passable
			sand, richland, poorland, dirt, field,
	};
	const static struct {u8 r,g,b;} colors[] = {
		{20, 150, 154},{201, 162, 62},{117, 167, 83},{130, 150, 88},{167, 134, 88},{160, 166, 65}
	};
	enum class dir { n, w, s, e, none };
	bool inrange(u8 x1, u8 y1, u8 x2, u8 y2, u8 range) {
		return (range*range) > ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1));
	}
	struct chunk {
		const static u8 size = 100;
		tile tiles[size * size];
		list<entity> entities;
		chunk* links[4];
		#define ta(x,y) (tiles[x + y*size])
		bool nearby(u8 x, u8 y, u8 dist, tile search) {
			const u8
				lx = (dist>x ? 0 : x-dist),
				ux = (x+dist>size ? size : x+dist),
				ly = (dist>y ? 0 : y-dist),
				uy = (y+dist>size ? size : y+dist);
			for (rpx y = ly; y < uy; ++y) for (rpx x = lx; x < ux; ++x)
				if (ta(x,y) == search) return true;
			return false;
		}
		entity* nearby(u8 x, u8 y, enum entity::kind k, u8 dist) {
			for (entity& e : entities) {
				if (e.kind == k and inrange(x,y,e.x,e.y,dist)) return &e;
			}
			return nullptr;
		}
		void _irrigate() {
			for (rpx y = 0; y < size; ++y) {
				for (rpx x = 0; x < size; ++x) {
					if (ta(x,y) != tile::water) { // eligible for upgrade?
						if (nearby(x,y,2,tile::water))
							ta(x,y) = tile::sand;
						else if (nearby(x,y,8,tile::water))
							ta(x,y) = tile::richland;
						else if (nearby(x,y,21,tile::water))
							ta(x,y) = tile::poorland;
					}
				}
			}
		}
		void gen(chunk* orig, dir orig_dir) {
			for (rpx y = 0; y < size*size; ++y) {
				tiles[y] = tile::dirt;
			}
			u8 water = rand()%4;
			for (u8 i = 0; i<water; ++i) {
				u8 len = rand()%5+1;
				u8 px = rand()%size;
				u8 py = rand()%size;
				while (len>0) {
					(rand()%2 ? px : py) += (rand()%2 ? 1 : -1);
					if (px >= size or py >= size) break;
					// handles overflows in either direction, by a happy accident
					ta(px,py) = tile::water;
				}
				_irrigate();
			}
			for (u8 i = 0; i<4; ++i) {
				if (i != (u8)orig_dir) links[i] = nullptr;
				else links[i] = orig;
			} // i'd be happier with self-modifying code here
			u8 trees = rand() % 200;
			for (u8 i = 0; i<trees; ++i) {
				u8 px = rand()%size;
				u8 py = rand()%size;
				if (ta(px,py) == tile::poorland) {
					entity& e = ++entities;
					e.x = px; e.y = py;
					e.kind = entity::kind::tree;
				} else if (ta(px,py) == tile::richland) {
					entity& e = ++entities;
					e.x = px; e.y = py;
					e.kind = entity::kind::bigtree;
				}
			}
		}
		#undef ta
	};
	entity player;
	chunk* place;
	void init() {
		place = new chunk;
		place->gen(nullptr,dir::none);
		player.kind=entity::kind::person;
		player.x=10;
		player.y=10;
	}

	void update() {
		if (glfwGetKey(runtime::window, GLFW_KEY_RIGHT)) player.x += 1;
		if (glfwGetKey(runtime::window, GLFW_KEY_UP)) player.y -= 1;
		if (glfwGetKey(runtime::window, GLFW_KEY_LEFT)) player.x -= 1;
		if (glfwGetKey(runtime::window, GLFW_KEY_DOWN)) player.y += 1;
	}
	void draw() {
		const float scale = 2;
		const rpx tilesize = 10, realsize = tilesize*scale;
		const rpx size = tilesize*scale * chunk::size;
		signed short oy, ox;
		if (size <= runtime::width) {
			ox = runtime::width / 2 - size / 2;
		} else ox = 0; //
		if (size <= runtime::height) {
			oy = runtime::height / 2 - size / 2;
		} else oy = 0; // FIXME calc for player pos
		for (rpx y = 0; y < chunk::size; ++y) {
			for (rpx x = 0; x < chunk::size; ++x) {
				tile t = place -> tiles[x + y*chunk::size];
				glColor3ub(colors[(size_t)t].r, colors[(size_t)t].g, colors[(size_t)t].b);
				glRecti(	x*realsize+ox,			(runtime::height-y*realsize)-oy,
							(x+1)*realsize+ox,		(runtime::height-(y+1)*realsize)-oy);
			}
		}
		/* draw player */ {
			glColor3ub(0,0,0);
			const rpx rx = player.x*realsize+ox, ry = (runtime::height-player.y*realsize)-oy,
					ex = (player.x+1)*realsize+ox, ey = (runtime::height-(player.y+1)*realsize)-oy;
			glRecti(rx,ry,ex,ey);
		}
		for (auto& e : place->entities) {
			const rpx rx = e.x*realsize+ox, ry = (runtime::height-e.y*realsize)-oy,
				ex = (e.x+1)*realsize+ox, ey = (runtime::height-(e.y+1)*realsize)-oy;
			switch (e.kind) {
				case entity::kind::tree:
					glColor3ub(114, 187, 115);
					glRecti(rx+(1*scale),ry+(2*scale),ex-(2*scale),ey-(2*scale));
					break;
				case entity::kind::bigtree:
					glColor3ub(114, 187, 115);
					glRecti(rx+(12*scale),ry+(12*scale),ex-(12*scale),ey-(12*scale));
					break;
			}
		}
		for (auto& e : place->entities) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			if (inrange(player.x, player.y, e.x, e.y, 2)) {
				const rpx rx = e.x*realsize+ox, ry = (runtime::height-e.y*realsize)-oy + 30,
					ex = (e.x+1)*realsize+ox, ey = (runtime::height-(e.y+1)*realsize)-oy + 30;
				font::bounds_t b = font::bounds(4,entity::names[(size_t)e.kind]);
				glColor4ub(0,0,0,127);
				glRecti(rx-10,ry+10,10+rx+b.x,ry-b.y-10);
				glColor3ub(0xff,0xff,0xff);
				font::drawstring(rx,ry,rx+b.x,rx+b.y,4,entity::names[(size_t)e.kind]);
			}
			glDisable(GL_BLEND);
		}
	}
}

int main() {
	srand(time(NULL));
	game::init();
	using namespace runtime;
	init(1024,640);
	loop();
}
